FTPMODE=auto
MAIL=/usr/mail/${LOGNAME:?}
MANPATH=/opt/local/gcc47/man:/opt/local/java/sun6/man:/opt/local/lib/perl5/man:/opt/local/lib/perl5/vendor_perl/man:/opt/local/man:/usr/share/man
PAGER=less
PATH=/opt/local/bin:/opt/local/sbin:/usr/bin:/usr/sbin:/opt/local/ruby/jruby-1.7.4/bin
JRUBY_OPTS='--1.9 --server -Xinvokedynamic.constants=true -J-Xmx4096m'

export FTPMODE MAIL MANPATH PAGER PATH JRUBY_OPTS

# hook man with groff properly
if [ -x /opt/local/bin/groff ]; then
  alias man='TROFF="groff -T ascii" TCAT="cat" PAGER="less -is" /usr/bin/man -T -mandoc'
fi

# help ncurses programs determine terminal size
export COLUMNS LINES

HOSTNAME=`/usr/bin/hostname`
HISTSIZE=1000
