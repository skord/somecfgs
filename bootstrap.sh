#!/usr/bin/env bash

pkgin -y update
pkgin -y full-upgrade
pkgin -y install build-essential
pkgin -y install haproxy nginx openjdk7 postgresql92-server redis scmgit
mkdir /opt/local/ruby
curl -O -L http://jruby.org.s3.amazonaws.com/downloads/1.7.4/jruby-bin-1.7.4.tar.gz
tar xvfz jruby-bin-1.7.4.tar.gz -C /opt/local/ruby
export PATH=/opt/local/ruby/jruby-1.7.4/bin:$PATH
echo "export PATH=/opt/local/ruby/jruby-1.7.4/bin:$PATH" >> ~/.profile
rm jruby-bin-1.7.4.tar.gz
git clone https://skord@bitbucket.org/skord/somecfgs.git
cp somecfgs/haproxy.cfg /opt/local/etc/haproxy.cfg
cp somecfgs/nginx.conf /opt/local/etc/nginx/nginx.conf
export JRUBY_OPTS='--1.9 --server -Xinvokedynamic.constants=true -J-Xmx1024m'
echo "export JRUBY_OPTS='--1.9 --server -Xinvokedynamic.constants=true -J-Xmx1024m'" >> ~/.profile
source ~/.bash_profile
svcadm enable haproxy
svcadm enable nginx
svcadm enable postgresql
svcadm enable redis
git clone https://skord@bitbucket.org/skord/stacktest.git
cd stacktest
gem install bundler
bundle
RAILS_ENV=production rake db:create
RAILS_ENV=production rake db:migrate
RAILS_ENV=production rake db:seed
RAILS_ENV=production rake tmp:create
bundle exec puma -e production -b unix:///root/stacktest/tmp/sockets/.puma.sock -t 8:16
